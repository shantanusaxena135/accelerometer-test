var win = Ti.UI.createWindow({
  backgroundColor: '#7F9FBF',
  exitOnClose:true,
  layout: 'vertical'
});


opts = {
  color:'black',
  font:{fontSize:20},
  text:'-',
  top:20, left:10,
  width:300
};

var xf =1, yf=1,zf=1,xa=1,ya=1,za=1,k=20, previoustimestamp=0;
var filecount=0;


var labelTimestamp = Ti.UI.createLabel(opts);
win.add(labelTimestamp);
var labelx = Ti.UI.createLabel(opts);
win.add(labelx);
var labely = Ti.UI.createLabel(opts);
win.add(labely);
var labelz = Ti.UI.createLabel(opts);
win.add(labelz);
var labelmodel2 =Ti.UI.createLabel(opts);
win.add(labelmodel2);
var labelstring =Ti.UI.createLabel(opts);
labelstring.color="red";
win.add(labelstring);
win.open();



  var accelerometerCallback = function(e) {
  labelTimestamp.text = 'timestamp: ' + e.timestamp;
  xf = k * xf + (1.0 - k) * e.x;
  yf = k * yf + (1.0 - k) * e.y;
  zf = k * zf + (1.0 - k) * e.z;
  

 // xa=e.x%5 + e.x/k;
  //ya=e.y%5 + e.y/k;
  //za=e.z%5 + e.z/k;
  

  labelmodel2.text= "no change detected";
  if((Math.abs(previoustimestamp-e.timestamp)>1000))
  {
 // 	alert("changed")
  	xa=e.x;
  	ya=e.y;
  	za=e.z;
    labelmodel2.text="IInd manipulation x:"+ xa + "  IInd manipulation y:" + ya + "   IInd manipulation z:" + za + "\n";
    previoustimestamp=e.timestamp;
  }
   
  
  labelx.text = 'originalx: ' + e.x + "  I manipulatedx:" + xf;
  labely.text = 'originaly: ' + e.y + "  I manupulatedy:" + yf;
  labelz.text = 'originalz: ' + e.z + "  I manipulatedz:" + zf;
  
  
  var StringForFile = e.x + "," + e.y + "," + e.z + "\n";
  
  
  var xyz = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory, 'xyz.csv');
  xyz.write(StringForFile,true);
  
  labelstring.text = "location:  " + xyz.nativePath;
  
};



if (Ti.Platform.model === 'Simulator' || Ti.Platform.model.indexOf('sdk') !== -1 ){
  alert('Accelerometer does not work on a virtual device');
} else {
  Ti.Accelerometer.addEventListener('update', accelerometerCallback);
  if (Ti.Platform.name === 'android'){
    Ti.Android.currentActivity.addEventListener('pause', function(e) {
      Ti.API.info("removing accelerometer callback on pause");
      Ti.Accelerometer.removeEventListener('update', accelerometerCallback);
    });
    Ti.Android.currentActivity.addEventListener('resume', function(e) {
      Ti.API.info("adding accelerometer callback on resume");
      Ti.Accelerometer.addEventListener('update', accelerometerCallback);
    });
  }
}




